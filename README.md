Para hacer funcionar Vocabuloso en un entorno local es imprescindible contar con un servidor web tipo XAMPP.

Si aún no lo tiene, puede descargarlo en el enlace siguiente: 

https://www.apachefriends.org/download.html

Se elige la versión adecuada para su sistema operativo y se siguen las instrucciones de instalación.

Vocabuloso ha sido desarrollado utilizando Symfony 4, framework de aplicaciones web desarroladas con PHP 
(si quiere saber un poco más sobre Symfony puede leer el enlace siguiente https://symfony.com/).

Debido a que Symfony usa composer para gestionar sus dependencias, si todavía no tiene composer, 
descárguelo siguiendo las instrucciones en:

http://getcomposer.org/ 

o sólo ejecute la siguiente orden desde consola:

curl -s http://getcomposer.org/installer | php

Vocabuloso implementa un control de versiones basado en Git, si no lo tiene instalada en su equipo, 
puede seguir las instrucciones de este enlace: 

https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/


Si todo ha ido bien, a esta altura ya cuentan con php versión > 7.1.x, MariaDB > 10.4.x, apache > 2.4.x., composer > 1.9.x, Git 2.24.x.
Si no los tiene, revise por favor las anteriores instrucciones.

También resulta cómodo definir php como variable de entorno, para eso (si todavía no lo tiene hecho en su equipo)
puede, en windows, seguir los pasos siguientes:

Hacer clic derecho en el icono de windows en la esquina inferior de la pantalla.
Elegir el menú Sistema.
Elegir la opción Infomación del Sistema  que está en la columna derecha.
Hacer clic en Configuración avanzada del Sistema.
Hacer clic en Variables de entorno.
Encontrar y seleccionar la variable PATH que está en la sección Variables del sistema.
Hacer clic en Editar.
Hacer clic en Nuevo.
Introducir la ruta hasta php (si ha instalado xampp en el directorio raíz será C:\xampp\php).
Hacer clic en Aceptar en esta ventana y las otras que se fueron abriendo (en total son 3).

Ahora viene lo interesante:
Se elige una carpeta raíz en donde se clonará la carpeta vocabuloso.

Al descargar Git, viene también el programa Git Bash que es una consola de líneas de comando que funciona como la consola de linux en windows.

Si utiliza Windows o Mac, se abre Git Bash y se ejecuta lo siguiente:

cd [ruta hasta la carpeta raíz] (por ejemplo: /c/Users/usuario/Documents/proyectos/)

git clone https://gitlab.com/rachid_el_madidi/vocabuloso.git

Se utiliza composer para instalar las dependencias de Vocabuloso, ejecutar:

cd vocabuloso

Para ejecutar composer, utilizar el siguiente comando:

php /c/xampp/php/composer.phar install

Ya tiene el código en local ahora toca crear la base de datos.

Abrir el programa XAMPP para acceder al panel de control

Hacer clic en los botones Start de Apache y MySQL

Hacer clic en el botón Admin de MySQL para abrir phpMyAdmin (o puede introduir http://localhost/phpmyadmin/ en el navegador)

Hacer clic en la pestaña Bases de datos.

Introducir vocabuloso en el campo nombre y hacer clic en el botón crear.

Desde la consola, ejecutar los archivos de migración:

php bin/console doctrine:migrations:migrate

En phpMyAdmin, seleccionar la base de datos vocabuloso.

Hacer clic en la pestaña SQL.

Ejecutar las siguientes queries:

INSERT INTO `idioma` (`id`, `nombre`, `bandera`) VALUES (NULL, 'español', 'es.png'), (NULL, 'francés', 'fr.png'), (NULL, 'inglés', 'en.png'), (NULL, 'alemán', 'de.png'), (NULL, 'japonés', 'ja.png');
INSERT INTO `nivel` (`id`, `nombre`) VALUES (NULL, 'a1'), (NULL, 'a2'), (NULL, 'b1'), (NULL, 'b2'), (NULL, 'c1'), (NULL, 'c2'), (NULL, 'otros');

En la consola, arrancar el servidor ejecutando el comando siguiente:

php bin/console server:run

Ya puede ver la aplicación introduciendo en el navegador http://localhost:8000.

Para disfrutar de la experiencia, ES NECESARIO iniciar sesión haciendo clic en el menú que lleva el mismo nombre y 
crear un perfil introduciendo un mail y un nombre de usuario.

Et voilà...a Vocabulosear!!!