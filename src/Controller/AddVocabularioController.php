<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Form\FiltrosType;
use App\Form\ListadoType;

use App\Entity\Filtros;
use App\Entity\Traduccion;
use App\Entity\Idioma;
use App\Entity\Listado;


class AddVocabularioController extends AbstractController
{
    /**
     * @Route("/add/filtros", name="add_filtros")
     */
    public function crearListado(Request $request, ObjectManager $manager)
    {
        // se crea un nuevo objeto filtros
        $filtros = new Filtros();
        
        // se le asigna el usuario actual como autor
        $filtros->setAutor($this->getUser());
        
        // se crea un formulario cuyos campos serán los atributos del objeto filtros
        $form = $this->createForm(FiltrosType::class, $filtros);
        
        // se recuperan los datos que han sido enviados
        $form->handleRequest($request);
        
        
        
        // si el formulario ha sido enviado y los campos son válidos
        if( $form->isSubmitted() && $form->isValid() ) 
        {
            // se guarda el filtro en la base de datos
            $manager->persist($filtros);
            $manager->flush();
            
            $filtrosId = $filtros->getId();
            
            // se redirecciona hacia la página que muestra el formulario del nuevo listado
            return $this->redirectToRoute('add_vocabulario', ['f' => $filtrosId]);            
        }
        
        // se muestra el formulario con los filtros
        return $this->render('add_vocabulario/filtros.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/add/vocabulario", name="add_vocabulario")
     */
    public function guardarListado(Request $request, ObjectManager $manager)
    {
        // se prepara el listado
        $listado = $this->prepararListado((int)$request->query->get('f'));
        
        // se vincula el formulario con el objeto listado
        $form = $this->createForm(ListadoType::class, $listado);
        
        // se recuperan los datos del formulario
        $form->handleRequest($request);
        
        // si el formulario ha sido enviado y los campos son válidos
        if( $form->isSubmitted() && $form->isValid() ) {
            // se guarda el listado en base de datos
            $manager->persist($listado);
            $manager->flush();
            
            // se redirige a la página de inicio
            return $this->redirectToRoute('home');            
        }
        
        // se muestra un formulario de listado vacío con todos los parámetros estipulados en el filtro recién creado 
        return $this->render('add_vocabulario/listado.html.twig', [
            'form' => $form->createView(),
            'filtros' => $this->serializarFiltros($listado->getFiltros())
        ]);
    }
    
    /*
     * crea un listado a partir del id del filtro que se ha escogido en el 
     * paso 1 para generarlo
     * @param int id de filtros
     * @return Listado
     */
    protected function prepararListado(int $id):Listado {
        // se crea un objeto de tipo Listado para asociarlo con los campos del formulario
        $listado = new Listado();
        
        // se recupera el objeto Filtros de la base de datos gracias al id de filtros
        $filtros = $this->getDoctrine()
        ->getRepository(Filtros::class)
        ->find($id);
  
        // asignamos al listado los filtros escogidos para su generación
        $listado->setFiltros($filtros);

        // se crea el número deseado de objetos de tipo Traduccion
        for( $i=1; $i <= $filtros->getCantidad(); $i++ ) {
            $listado->addTraduccion(new Traduccion());
        }
        return $listado;
    }
    
    /*
     * Devuelve el objeto filtros convertido en un array
     * @param Filtros filtros que han servido para la generación del listado de vocabulario
     * @return array 
     */
    function serializarFiltros(Filtros $filtros) {
        $f = array();
        $f['nivel'] = $filtros->getNivel()->getNombre();
        $f['categoria'] = $filtros->getCategoria()->getNombre();
        $f['idioma1'] = $filtros->getIdioma1()->getBandera();
        $f['idioma2'] = $filtros->getIdioma2()->getBandera();
        return $f;
    }
    
    
    /**
     * Devuelve una cadena en formato json con los posibles idiomas para el campo idioma destino
     * @Route("/get-idiomas-traduccion", name="actualizar_idiomas")
     * @param Request $request
     * @return JsonResponse
     */
    public function actualizarIdiomas(Request $request)
    {
        // Se recupera el la clase IdiomaRepository para poder acceder a los idiomas almacenados en base de datos
        $repository = $this->getDoctrine()->getRepository(Idioma::class);
        // se buscan los posibles idiomas excluyendo el idoma que ya se eligió y que pasamos como parámetro idioma1 en la petición
        $posiblesIdiomas = $repository->getIdiomaTraduccion($request->query->get('i'));
        
        // creamos un array con todos los valores
        $idiomas = array();
        foreach($posiblesIdiomas as $posiblesIdioma){
            $idiomas[] = array(
                "id" => $posiblesIdioma->getId(),
                "name" => ucfirst($posiblesIdioma->getNombre())
            );
        }
        
        // Devuelve los posibles idiomas en formato json
        return new JsonResponse($idiomas);
    }
}
