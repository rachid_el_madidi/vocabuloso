<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Traduccion;
use App\Entity\Filtros;
use App\Entity\Idioma;
use App\Entity\Listado;

use App\Form\RepasarType;

class RepasarController extends AbstractController
{
    /**
     * @Route("/repasar", name="repasar")
     */
    public function index(Request $request)
    {
        $filtros = new Filtros();
        
        $filtros->setAutor($this->getUser());
        
        // prever aquí el caso user no autenticado redireccionar al login
        // si no hay ningún listado creado se muestra un mensaje de error
        if (count($this->getDoctrine()->getRepository(Filtros::class)->findAll()) < 1) {
            throw $this->createNotFoundException('Hace falta primero crear un listado antes de poder repasar');
            
        }
        
        $idioma1s = array_values($this->getDoctrine()->getRepository(Filtros::class)->idioma1PorAutor($filtros->getAutor()->getId())); 
        
        $idiomas = array();
        foreach ($idioma1s as $i1) {
            $idiomas[] = $i1['id'];
        }
        $i = $this->getDoctrine()->getRepository(Idioma::class)->idioma1PorAutor($idiomas);        

        $form = $this->createForm(RepasarType::class, $filtros, ["idiomas" => $i, "autor" => $filtros->getAutor()]);

        $form->handleRequest($request);

        // si el formulario ha sido enviado y los campos son válidos
        if( $form->isSubmitted() && $form->isValid() ) {
            $d = $this->getDoctrine();
            // recuperar los filtros que han servido para crear el listado
            $f = $d->getRepository(Filtros::class)
                ->findFiltrosRepasar($filtros->getIdioma1(), $filtros->getIdioma2(), $filtros->getNivel(), $filtros->getCategoria(), $filtros->getAutor());

            // recuperamos el listado si existen los filtros
            if($f) {
                $l = $d->getRepository(Listado::class)
                    ->FindOneByFiltrosId($f[0]['id']);
                // recuperamos las traducciones si existe el listado
                if($l) {
                    $trads = $d->getRepository(Traduccion::class)
                            ->getTradByListId($l);  
                    // recuperamos las palabras si existen las traducciones
                    if($trads) {
                        $palabras = array();
                        for( $i = 0; $i < count($trads); $i ++ ) {
                            $palabras[$i]["p1"] = $trads[$i]->getPalabra1()->getValor();
                            $palabras[$i]["p2"] = $trads[$i]->getPalabra2()->getValor();
                        }

                        return $this->render('repasar/listado.html.twig', [
                            'palabras' => $palabras,
                            'filtros' => $filtros,
                            'resultados' => true
                        ]);
                    }
                }
            }

            return $this->render('repasar/listado.html.twig', [
                'resultados' => false
            ]);


        }        
        return $this->render('repasar/repasar.html.twig', [
            'form' => $form->createView()
        ]);
         

        
    }
    
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        $this->traduccionesPorIdiomas();
        return $this->render('repasar/home.html.twig', [
            'palabras_dia' => $this->palabrasDelDia(5), // array con las palabras del día
            'graf_nivel' => $this->palabrasPorNivel() // array con la cantidad de palabras por niveles
        ]);
    }    
    
    protected function palabrasDelDia(int $numPalabras):array 
    {
        // se obtienen traducciones al azar
        $tradsAlAzar = $this->getDoctrine()
                ->getRepository(Traduccion::class)
                ->palabrasAlAzar($numPalabras)
        ; 
        
        // creamos un array con todos los valores
        $traducciones = array();
        
        foreach($tradsAlAzar as $tradAlAzar){
            $traducciones[] = array(
                "palabra1" => $tradAlAzar->getPalabra1()->getValor(),
                "idioma1" => $tradAlAzar->getListado()->getFiltros()->getIdioma1()->getBandera(),
                "palabra2" => $tradAlAzar->getPalabra2()->getValor(),
                "idioma2" => $tradAlAzar->getListado()->getFiltros()->getIdioma2()->getBandera()
            );
        }     
        
        return $traducciones;
    }
    
    protected function palabrasPorNivel()
    {
        $palPorNiv = $this->getDoctrine()
                ->getRepository(Filtros::class)
                ->palabrasPorNivel()
        ;
        
        $niveles = array();
        
        foreach($palPorNiv as $niv) {
            $niveles[] = array(
                "nombre" => $niv['nombre'],
                "valor" => $niv['cantidad']*2
            ); 
        }
        return $niveles;
    }
    
    protected function traduccionesPorIdiomas()
    {
        $rep = $this->getDoctrine()
                ->getRepository(Filtros::class);
        $tradPorIds = $rep->traduccionesPorIdiomas();
        
        $traducciones = array();
        
        foreach($tradPorIds as $trad) {
            //$nombre = $trad['nombre'].'-'.$rep->idiomaPorId($trad['id']);var_dump($nombre);die;

            $traducciones[] = array(
                "nombre" => "",
                "valor" => $trad['cantidad']
            ); 
        }
        return $traducciones;
    }
    
    /**
     * Devuelve una cadena en formato json con los posibles idiomas para el campo idioma destino
     * @Route("/actu/idiomas", name="actu_idiomas")
     * @param Request $request
     * @return JsonResponse
     */
    public function actuIdiomas(Request $request)
    {
        // Se recupera el la clase IdiomaRepository para poder acceder a los idiomas almacenados en base de datos
        $d = $this->getDoctrine();
        $repository = $d->getRepository(Filtros::class);
        // se buscan los posibles idiomas excluyendo el idoma que ya se eligió y que pasamos como parámetro idioma1 en la petición
        $posiblesIdiomas = $repository->getIdioma2ByIdioma1((int)$request->query->get('i'), $this->getUser()->getId());

        // creamos un array con todos los valores
        $idiomas = array();
        foreach ($posiblesIdiomas as $idioma2) {
            $aux = $d->getRepository(Idioma::class)->find($idioma2['id']);
            $idiomas[] = array(
                "id" => $aux->getId(),
                "name" => ucfirst($aux->getNombre())
            );
        }        
        
        // Devuelve los posibles idiomas en formato json
        return new JsonResponse($idiomas);
    }
    
    
    /**
     * Devuelve una cadena en formato json con los posibles niveles para el campo idioma destino
     * @Route("/actu/niveles", name="actu_niveles")
     * @param Request $request
     * @return JsonResponse
     */
    public function actuNiveles(Request $request)
    {
        // Se recupera el la clase IdiomaRepository para poder acceder a los idiomas almacenados en base de datos
        $d = $this->getDoctrine();
        $repository = $d->getRepository(Filtros::class);
        // se buscan los posibles idiomas excluyendo el idoma que ya se eligió y que pasamos como parámetro idioma1 en la petición
        $posiblesNiveles = $repository->getNiveles($request->query->get('i1'), $request->query->get('i2'));
        
        // Devuelve los posibles niveles en formato json
        return new JsonResponse($posiblesNiveles);
    }    
    
    /**
     * Devuelve una cadena en formato json con las posibles categorias para el campo categoria
     * @Route("/actu/categorias", name="actu_categorias")
     * @param Request $request
     * @return JsonResponse
     */
    public function actuCategorias(Request $request)
    {       
        // Se recupera la clase FiltrosRepository para poder acceder a las categorias almacenadas en base de datos
        $d = $this->getDoctrine();
        $repository = $d->getRepository(Filtros::class);
        // se buscan las posibles categorias que coinciden con los idiomas y el nivel que recibimos como parámetros en la petición
        $posiblesCategorias = $repository->getCategorias($request->query->get('i1'), $request->query->get('i2'), $request->query->get('n'));              

        // Devuelve las posibles categorías en formato json
        return new JsonResponse($posiblesCategorias);
    }    
    
}
