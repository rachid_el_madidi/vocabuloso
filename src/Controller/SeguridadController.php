<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Autor;
use App\Form\RegistracionType;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SeguridadController extends AbstractController
{
    /**
     * @Route("/registracion", name="seguridad_registracion")
     */
    public function registracion(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $autor = new Autor();
        
        $form = $this->createForm(RegistracionType::class, $autor);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            // se encripta la contraseña antes de almacenarla en la base de datos
            $hash = $encoder->encodePassword($autor, $autor->getPassword());
            $autor->setPassword($hash);
            
            // se guarda el usuario en la base de datos
            $manager->persist($autor);
            $manager->flush();
            
            // una vez registrado se redirige a la página de login
            return $this->redirectToRoute('seguridad_login');
        }
        
        return $this->render('seguridad/registracion.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/login", name="seguridad_login")
     */
    public function login() {
        return $this->render('seguridad/login.html.twig');
    }
    
    /**
     * @Route("/logout", name="seguridad_logout")
     */
    public function logout() {}    
}
