<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Filtros;
use App\Entity\Idioma;
use App\Entity\Test;
use App\Entity\Pregunta;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RepasarType;
use App\Form\TestType;

class TestController extends AbstractController
{
    /**
     * @Route("/test/ver", name=Test::RUTA_EXAMENES)
     * @Route("/test", name="test_filtros")
     */
    public function index(Request $request, ObjectManager $manager)
    {
        $filtros = new Filtros();
        
        if(!$this->getUser()) {
            return $this->render('test/index.html.twig', [
                'conectado' => false
            ]);
        }
        
        // comprueba si la ruta es la de ver los exámenes
        $ver = ($request->attributes->get('_route') == Test::RUTA_EXAMENES);
        
        // asigna el usario actual a los filtros
        $filtros->setAutor($this->getUser());
        
        // creación del formulario de filtros pasándole como parámetro los idiomas1 y el autor
        $form = $this->createForm(RepasarType::class, $filtros, ["idiomas" => $this->obtenerIdiomas1($filtros), "autor" => $filtros->getAutor()]);

        $form->handleRequest($request);

        // si el formulario ha sido enviado y los campos son válidos
        if( $form->isSubmitted() && $form->isValid() ) {
            
            $d = $this->getDoctrine();
            // recuperar los filtros que han servido para crear el listado
            $f = $d->getRepository(Filtros::class)
                ->findFiltrosRepasar($filtros->getIdioma1(), $filtros->getIdioma2(), $filtros->getNivel(), $filtros->getCategoria(), $filtros->getAutor());
            
            if(!$f) {
                return new Response('Hubo un problema con estos filtros');
            }
            // si la ruta es la de ver exámenes
            if($ver) {
                return $this->redirectToRoute("ver_examen", ["f" => $f[0]["id"]]);
            }
            
            return $this->redirectToRoute("test_examen", ["f" => $f[0]["id"]]);
        }        
        return $this->render('test/filtrosTest.html.twig', [
            'form' => $form->createView(),
            'conectado' => true,
            'ver' => $ver
        ]);
    }
    
    /**
     * @Route("/test/examen", name="test_examen")
     * @param int $id
     * @return Test
     */
    public function examen(Request $request, ObjectManager $manager)
    {
        // preparamos el test si existen los filtros
        $test = $this->prepararTest((int)$request->query->get('f'));//var_dump($test->getPreguntas()[0]->getTraduccion()->getPalabra1()->getValor());die;

        // se crea un formulario y se le vincula al objeto test
        $formTest = $this->createForm(TestType::class, $test);
        
        $formTest->handleRequest($request);

        if( $formTest->isSubmitted() && $formTest->isValid() ) {
            // fecha y hora de la realización del test
            $test->setRealizado(new \DateTime);

            // se calcula la nota
            $test->setNota($this->calcularNota($test));
            
            // se guarda en base de datos
            $manager->persist($test);
            $manager->flush();
            
            // se redirige a la página de resultados de los tests
            return $this->redirectToRoute("ver_examen", ["f" => $request->query->get('f')]);
        }
        
        return $this->render('test/test.html.twig', [
            'form' => $formTest->createView(),
            'filtros' => $this->getFiltrosById($request->query->get("f"))
        ]);
    }
    
    public function prepararTest(int $id):Test
    {
        // se crea un objeto de tipo Test para asociarlo con los campos del formulario
        $test = new Test();
        
        // se recupera el objeto Filtros de la base de datos gracias al id de filtros
        $filtros = $this->getFiltrosById($id);
    
        // asignamos al test la fecha de creación y los filtros escogidos para la generación del listado sobre él que trata
        $test->setCreado(new \DateTime);
        $test->setFiltros($filtros);
        
        // recuperamos las traducciones del listado
        $traducciones = $this->getTraduccioneByFiltro($filtros);
        
        // se asignan al test las preguntas con las traducciones
        foreach($traducciones as $traduccion) {
            $pregunta = new Pregunta();
            $pregunta->setTraduccion($traduccion);
            $test->addPregunta($pregunta);
        }
        return $test;
    }
    
    public function getFiltrosById($id):Filtros
    {
        return $this->getDoctrine()
        ->getRepository(Filtros::class)
        ->find($id);
    }
    
    public function getTraduccioneByFiltro(Filtros $filtros):Collection
    {
        $traducciones = new ArrayCollection;
        // recuperamos el listado
        $listado = $filtros->getListado();

        // recuperamos las traducciones si existe el listado
        if($listado) {                
            $traducciones= $listado->getTraducciones(); 
        }
        return $traducciones;
    }
    
    private function calcularNota(Test $test):string
    {
        $nota = 0;
        // se recuperan las preguntas
        $preguntas = $test->getPreguntas();
        
        foreach($preguntas as $pregunta)
        {
            if($pregunta->getRespuesta() == $pregunta->getTraduccion()->getPalabra2()->getValor()) {
                ++$nota;
            }
        } 
        return (string)(Test::NOTA_MAXIMA * $nota/count($preguntas));
    }
        
    
    /**
     * @Route("/test/ver/examen", name="ver_examen")
     */
    public function verExamenes(Request $request)
    {
        // se recupera el objeto Filtros de la base de datos gracias al id de filtros
        $filtros = $this->getFiltrosById($request->query->get("f"));
        
        // se recuperan todos los tests generados a partir de estos filtros
        $tests = $filtros->getTests();
        
        return $this->render('test/verExamenes.html.twig', [
            'tests' => $tests,
            'filtros' => $filtros,
        ]);        

    }
    
    /**
     * @Route("test/ver/detalles", name="ver_detalles_examen")
     * @param Filtros $filtros
     * @return array
     */
    public function verDetallesExamen(Request $request)
    {
        // recuperamos el test gracias al id recibido por post
        $test = $this->getDoctrine()->getRepository(Test::class)->find($request->request->get('testid'));
        
        // recuperamos las preguntas del test
        $preguntas = $test->getPreguntas();
        
        $preg = array();
        foreach($preguntas as $pregunta)
        {
            $preg[] = array("palabra1" => $pregunta->getTraduccion()->getPalabra1()->getValor(),
                            "palabra2" => $pregunta->getTraduccion()->getPalabra2()->getValor(),
                            "respuesta" => $pregunta->getRespuesta());
                            
        }
        
        // se muestra por pantalla el exámen con todos sus detalles
        return $this->render('test/verDetalles.html.twig', [
           'test' => $test,
            'preguntas' => $preg
        ]);
    }
    
    private function obtenerIdiomas1(Filtros $filtros):array
    {
        // obtener los posibles valores del campo idioma1 en los filtros perteneciendo al usuario actual
        $idioma1s = array_values($this->getDoctrine()->getRepository(Filtros::class)->idioma1PorAutor($filtros->getAutor()->getId()));
        
        $idiomas = array();
        foreach ($idioma1s as $i1) {
            $idiomas[] = $i1['id'];
        }
        return $this->getDoctrine()->getRepository(Idioma::class)->idioma1PorAutor($idiomas);
    }
}
