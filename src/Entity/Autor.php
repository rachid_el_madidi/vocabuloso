<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AutorRepository")
 * @UniqueEntity(fields="email", message="Este correo electrónico ya está utilizado")
 */
class Autor implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(
     *     message = "El correo electrónico '{{ value }}' no es válido.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(
     *      min = 4,
     *      max = 50,
     *      minMessage = "El nombre de usuario debe tener como mínimo  {{ limit }} carácteres de longitud",
     *      maxMessage = "El nombre de usuario debe tener como máximo {{ limit }} carácteres de longitud"
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 8,
     *      max = 255,
     *      minMessage = "La contraseña debe tener como mínimo  {{ limit }} carácteres de longitud",
     *      maxMessage = "La contraseña debe tener como máximo {{ limit }} carácteres de longitud"
     * )
     */
    private $password;
    
    /**
     * @Assert\EqualTo(propertyPath="password", message="Las contraseñas no coinciden")
     */
    private $confirm_password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Filtros", mappedBy="autor")
     */
    private $filtros;

    public function __construct()
    {
        $this->filtros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    
    public function getConfirmPassword(): ?string
    {
        return $this->confirm_password;
    }

    public function setConfirmPassword(string $confirm_password): self
    {
        $this->confirm_password = $confirm_password;

        return $this;
    }
    
    public function eraseCredentials() {}
    
    public function getSalt() {}
    
    public function getRoles() {
        return ['ROLE_USER'];
    }

    /**
     * @return Collection|Filtros[]
     */
    public function getFiltros(): Collection
    {
        return $this->filtros;
    }

    public function addFiltro(Filtros $filtro): self
    {
        if (!$this->filtros->contains($filtro)) {
            $this->filtros[] = $filtro;
            $filtro->setAutor($this);
        }

        return $this;
    }

    public function removeFiltro(Filtros $filtro): self
    {
        if ($this->filtros->contains($filtro)) {
            $this->filtros->removeElement($filtro);
            // set the owning side to null (unless already changed)
            if ($filtro->getAutor() === $this) {
                $filtro->setAutor(null);
            }
        }

        return $this;
    }
}
