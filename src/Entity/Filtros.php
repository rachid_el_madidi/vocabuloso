<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FiltrosRepository")
 */
class Filtros
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\Column(type="smallint")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Nivel", inversedBy="filtros")
     * @ORM\JoinColumn(nullable=false)
     */
    private $nivel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="filtros", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Idioma")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idioma1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Idioma")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idioma2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Autor", inversedBy="filtros")
     * @ORM\JoinColumn(nullable=false)
     */
    private $autor;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Listado", mappedBy="filtros", cascade={"persist", "remove"})
     */
    private $listado;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Test", mappedBy="filtros")
     */
    private $tests;

    public function __construct()
    {
        $this->tests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getNivel(): ?Nivel
    {
        return $this->nivel;
    }

    public function setNivel(?Nivel $nivel): self
    {
        $this->nivel = $nivel;

        return $this;
    }

    public function getCategoria(): ?Categoria
    {
        return $this->categoria;
    }

    public function setCategoria(?Categoria $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getIdioma1(): ?Idioma
    {
        return $this->idioma1;
    }

    public function setIdioma1(?Idioma $idioma1): self
    {
        $this->idioma1 = $idioma1;

        return $this;
    }

    public function getIdioma2(): ?Idioma
    {
        return $this->idioma2;
    }

    public function setIdioma2(?Idioma $idioma2): self
    {
        $this->idioma2 = $idioma2;

        return $this;
    }

    public function getAutor(): ?Autor
    {
        return $this->autor;
    }

    public function setAutor(?Autor $autor): self
    {
        $this->autor = $autor;

        return $this;
    }

    public function getListado(): ?Listado
    {
        return $this->listado;
    }

    public function setListado(Listado $listado): self
    {
        $this->listado = $listado;

        // set the owning side of the relation if necessary
        if ($listado->getFiltros() !== $this) {
            $listado->setFiltros($this);
        }

        return $this;
    }

    /**
     * @return Collection|Test[]
     */
    public function getTests(): Collection
    {
        return $this->tests;
    }

    public function addTest(Test $test): self
    {
        if (!$this->tests->contains($test)) {
            $this->tests[] = $test;
            $test->setFiltros($this);
        }

        return $this;
    }

    public function removeTest(Test $test): self
    {
        if ($this->tests->contains($test)) {
            $this->tests->removeElement($test);
            // set the owning side to null (unless already changed)
            if ($test->getFiltros() === $this) {
                $test->setFiltros(null);
            }
        }

        return $this;
    }
}
