<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ListadoRepository")
 */
class Listado
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Traduccion", mappedBy="listado", orphanRemoval=true, cascade={"persist"})
     */
    private $traducciones;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Filtros", inversedBy="listado", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $filtros;

    public function __construct()
    {
        $this->traducciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Traduccion[]
     */
    public function getTraducciones(): Collection
    {
        return $this->traducciones;
    }

    public function addTraduccion(Traduccion $traduccion): self
    {
        if (!$this->traducciones->contains($traduccion)) {
            $this->traducciones[] = $traduccion;
            $traduccion->setListado($this);
        }

        return $this;
    }

    public function removeTraduccion(Traduccion $traduccion): self
    {
        if ($this->traducciones->contains($traduccion)) {
            $this->traducciones->removeElement($traduccion);
            // set the owning side to null (unless already changed)
            if ($traduccion->getListado() === $this) {
                $traduccion->setListado(null);
            }
        }

        return $this;
    }

    public function getFiltros(): ?Filtros
    {
        return $this->filtros;
    }

    public function setFiltros(Filtros $filtros): self
    {
        $this->filtros = $filtros;

        return $this;
    }
}
