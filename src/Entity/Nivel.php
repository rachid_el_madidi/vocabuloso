<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NivelRepository")
 */
class Nivel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Filtros", mappedBy="nivel")
     */
    private $filtros;

    public function __construct()
    {
        $this->filtros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|Filtros[]
     */
    public function getFiltros(): Collection
    {
        return $this->filtros;
    }

    public function addFiltro(Filtros $filtro): self
    {
        if (!$this->filtros->contains($filtro)) {
            $this->filtros[] = $filtro;
            $filtro->setNivel($this);
        }

        return $this;
    }

    public function removeFiltro(Filtros $filtro): self
    {
        if ($this->filtros->contains($filtro)) {
            $this->filtros->removeElement($filtro);
            // set the owning side to null (unless already changed)
            if ($filtro->getNivel() === $this) {
                $filtro->setNivel(null);
            }
        }

        return $this;
    }
}
