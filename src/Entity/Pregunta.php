<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PreguntaRepository")
 */
class Pregunta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Test", inversedBy="preguntas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $test;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $respuesta;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Traduccion", inversedBy="preguntas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $traduccion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getRespuesta(): ?string
    {
        return $this->respuesta;
    }

    public function setRespuesta(string $respuesta): self
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    public function getTraduccion(): ?Traduccion
    {
        return $this->traduccion;
    }

    public function setTraduccion(?Traduccion $traduccion): self
    {
        $this->traduccion = $traduccion;

        return $this;
    }

}
