<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestRepository")
 */
class Test
{
    /**
     *  constante para definir la nota máxima que se puede sacar en un test
     */
    const NOTA_MAXIMA = 10;
    const RUTA_EXAMENES = "test_ver_filtros";
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Filtros", inversedBy="tests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $filtros;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creado;

    /**
     * @ORM\Column(type="datetime")
     */
    private $realizado;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $nota;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pregunta", mappedBy="test", cascade={"persist"})
     */
    private $preguntas;

    public function __construct()
    {
        $this->preguntas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFiltros(): ?Filtros
    {
        return $this->filtros;
    }

    public function setFiltros(?Filtros $filtros): self
    {
        $this->filtros = $filtros;

        return $this;
    }

    public function getCreado(): ?\DateTimeInterface
    {
        return $this->creado;
    }

    public function setCreado(\DateTimeInterface $creado): self
    {
        $this->creado = $creado;

        return $this;
    }

    public function getRealizado(): ?\DateTimeInterface
    {
        return $this->realizado;
    }

    public function setRealizado(\DateTimeInterface $realizado): self
    {
        $this->realizado = $realizado;

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(string $nota): self
    {
        $this->nota = $nota;

        return $this;
    }

    /**
     * @return Collection|Pregunta[]
     */
    public function getPreguntas(): Collection
    {
        return $this->preguntas;
    }

    public function addPregunta(Pregunta $pregunta): self
    {
        if (!$this->preguntas->contains($pregunta)) {
            $this->preguntas[] = $pregunta;
            $pregunta->setTest($this);
        }

        return $this;
    }

    public function removePregunta(Pregunta $pregunta): self
    {
        if ($this->preguntas->contains($pregunta)) {
            $this->preguntas->removeElement($pregunta);
            // set the owning side to null (unless already changed)
            if ($pregunta->getTest() === $this) {
                $pregunta->setTest(null);
            }
        }

        return $this;
    }
}
