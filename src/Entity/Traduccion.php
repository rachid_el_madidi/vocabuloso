<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TraduccionRepository")
 */
class Traduccion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Palabra", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $palabra1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Palabra", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $palabra2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Listado", inversedBy="traducciones", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $listado;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pregunta", mappedBy="traduccion")
     */
    private $preguntas;

    public function __construct()
    {
        $this->preguntas = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPalabra1(): ?Palabra
    {
        return $this->palabra1;
    }

    public function setPalabra1(?Palabra $palabra1): self
    {
        $this->palabra1 = $palabra1;

        return $this;
    }

    public function getPalabra2(): ?Palabra
    {
        return $this->palabra2;
    }

    public function setPalabra2(?Palabra $palabra2): self
    {
        $this->palabra2 = $palabra2;

        return $this;
    }

    public function getListado(): ?Listado
    {
        return $this->listado;
    }

    public function setListado(?Listado $listado): self
    {
        $this->listado = $listado;

        return $this;
    }

    /**
     * @return Collection|Pregunta[]
     */
    public function getPreguntas(): Collection
    {
        return $this->preguntas;
    }

    public function addPregunta(Pregunta $pregunta): self
    {
        if (!$this->preguntas->contains($pregunta)) {
            $this->preguntas[] = $pregunta;
            $pregunta->setTraduccion($this);
        }

        return $this;
    }

    public function removePregunta(Pregunta $pregunta): self
    {
        if ($this->preguntas->contains($pregunta)) {
            $this->preguntas->removeElement($pregunta);
            // set the owning side to null (unless already changed)
            if ($pregunta->getTraduccion() === $this) {
                $pregunta->setTraduccion(null);
            }
        }

        return $this;
    }
    
    /**
     * devuelve el valor de la palabara en el idioma de origen
     * @return String
     */
    public function __toString() {
        return $this->palabra1->getValor();
    }
}
