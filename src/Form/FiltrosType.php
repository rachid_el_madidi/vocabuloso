<?php

namespace App\Form;

use App\Entity\Filtros;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Nivel;
use App\Entity\Idioma;

use App\Form\CategoriaType;



class FiltrosType extends AbstractType
{
    protected $em;
    
    /**
     * The Type requires the EntityManager as argument in the constructor. It is autowired
     * in Symfony 3.
     * 
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categoria', CategoriaType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Título del listado de vocabulario'
                ]
            ])                
            ->add('idioma1', EntityType::class, [
                'class' => Idioma::class,
                'choice_label' => function(Idioma $idioma1) {
                    return sprintf('%s', ucfirst($idioma1->getNombre()));
                },
                'placeholder' => 'Seleccione un idioma',
                'label' => 'Idioma original'
            ])                      
            ->add('nivel', EntityType::class, [
                'class' => Nivel::class,
                'choice_label' => function(Nivel $nivel) {
                    return sprintf('%s', ucfirst($nivel->getNombre()));
                },
                'placeholder' => 'Seleccione un nivel'
            ])
            ->add('cantidad', TextType::class, [
                'required' => 'required',
                'attr' => [
                    'placeholder' => 'Número de términos'
                ]
            ]);
                
        // se añaden eventos al formulario para que los desplegables de los idiomas sean un combolist.        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));    
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }
    
    
    protected function addIdioma2(FormInterface $form, Idioma $idioma1 = null) 
    {
        // No se muestran los posibles idiomas para traducir el vocabulario hasta que se eliga el idioma del vocabulario original
        $idiomas = array();
        
        // si ya se ha elegido el idioma original, se muestran posibles idiomas para la traducción
        if ($idioma1) {
            // petición a base de datos
            $repository = $this->em->getRepository(Idioma::class);
            
            $idiomas = $repository->createQueryBuilder("q")
                ->where("q.id != :idioma1_id")
                ->setParameter("idioma1_id", $idioma1->getId())
                ->getQuery()
                ->getResult();
        }
        
        // Se añaden los idiomas
        $form->add('idioma2', EntityType::class, [
                'class' => Idioma::class,
                'choices' => $idiomas,
                'placeholder' => 'Seleccione un idioma',
                'label' => 'Idioma de la traducción'
        ]);
    }
    
    function onPreSubmit(FormEvent $event) 
    {
        $form = $event->getForm();
        $filtros = $event->getData();//var_dump($filtros['idioma1']);die;
        
        // se recupera el idioma original seleccionado para actualizar los posibles valores del otro idioma
        $idioma1 = $this->em->getRepository(Idioma::class)->find($filtros['idioma1']);
        
        $this->addIdioma2($form, $idioma1);
    }

    function onPreSetData(FormEvent $event) 
    {
        $filtros = $event->getData();
        $form = $event->getForm();

        // cuando se llega al paso 1 para añadir vocabulario, el idioma de destino aún no contendrá valores
        $idioma1 = $filtros->getIdioma1() ? $filtros->getIdioma1() : null;
        
        $this->addIdioma2($form, $idioma1);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filtros::class,
        ]);
    }
}
