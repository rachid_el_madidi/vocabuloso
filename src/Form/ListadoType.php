<?php

namespace App\Form;

use App\Entity\Listado;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Form\TraduccionType;

class ListadoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('traducciones', CollectionType::class, [
                // colección de objetos de tipo Traduccion
                'entry_type' => TraduccionType::class,
                // opciones de los objetos de tipo Traduccion
                'entry_options' => [
                    'attr' => [],
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Listado::class,
        ]);
    }
}
