<?php

namespace App\Form;


use App\Form\FiltrosType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;


use App\Entity\Idioma;
use App\Entity\Nivel;
use App\Entity\Filtros;
use App\Entity\Categoria;

class RepasarType extends FiltrosType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idioma1', EntityType::class, [
                'class' => Idioma::class,
                'choices' => $options['idiomas'],
                'choice_label' => function(Idioma $idioma1) {
                    return sprintf('%s', ucfirst($idioma1->getNombre()));
                },
                'placeholder' => 'Seleccione un idioma',
                'label' => 'Idioma original'
            ]) 
        ;
                
        // se añaden eventos al formulario para que los desplegables sean varios combolist.        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));    
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));                
    }
    
    function onPreSubmit(FormEvent $event) 
    {
        $form = $event->getForm();
        $filtros = $event->getData();
        //var_dump($filtros);die;
        
        $idioma1 = $this->em->getRepository(Idioma::class)->find($filtros['idioma1']); 
        
        $this->addIdioma2($form, $idioma1);
        $idioma2 = $this->em->getRepository(Idioma::class)->find($filtros['idioma2']);
        
        $this->addNivel($form, $idioma1, $idioma2);
        $nivel = $this->em->getRepository(Nivel::class)->find($filtros['nivel']);
        
        $this->addCategoria($form, $idioma1, $idioma2, $nivel);
    }

    function onPreSetData(FormEvent $event) 
    {
        $filtros = $event->getData();
        $form = $event->getForm();

        // el idioma de origen puede que esté ya seteado o no
        $idioma1 = $filtros->getIdioma1() ? $filtros->getIdioma1() : null;
        
        if($idioma1 != null) {

        }
            
        // se añade el campo idioma2 al formulario
        $this->addIdioma2($form, $idioma1);
                
        // el idioma de destino solo tiene valor si lo tiene también el idioma de origen
        $idioma2 = $filtros->getIdioma2() ? $filtros->getIdioma2() : null;
        
        // se añade el campo nivel al formulario
        $this->addNivel($form, $idioma1, $idioma2);
 
        // el campo nivel solo tiene valores si los tiene también el campo idioma de destino 
        $nivel = $filtros->getNivel() ? $filtros->getNivel() : null;   
        
        // se añade el campo categoria al formulario
        $this->addCategoria($form, $nivel);        
    }        
    
    protected function addIdioma2(FormInterface $form, Idioma $idioma1 = null) 
    {
        // No se muestran los posibles idiomas para traducir el vocabulario hasta que se eliga el idioma del vocabulario original
        $idiomas = array();
        $autor = $form->getConfig()->getOption("autor");
        
        // si ya se ha elegido el idioma original, se muestran posibles idiomas para la traducción
        if ($idioma1 && $autor) {
            // petición a base de datos
            $repository = $this->em->getRepository(Filtros::class);

            $idioma2s = $repository->getIdioma2ByIdioma1($idioma1->getId(), $autor->getId());
            
            foreach ($idioma2s as $idioma2) {
                $idiomas[] = $this->em->getRepository(Idioma::class)->find($idioma2['id']);
            }
        }
        
        // Se añaden los idiomas
        $form->add('idioma2', EntityType::class, [
                'class' => Idioma::class,
                'choices' => $idiomas,
                'choice_label' => function(Idioma $idioma1) {
                    return sprintf('%s', ucfirst($idioma1->getNombre()));
                },
                'placeholder' => 'Seleccione un idioma',
                'label' => 'Idioma de la traducción'
        ]);
    }    
    
    public function addNivel(FormInterface $form, Idioma $idioma1 = null, Idioma $idioma2 = null) {
        // No se muestran los posibles niveles hasta que se eliga el idioma al que se traducirá el vocabulario
        $niveles = array();
        
        // si ya se ha elegido el idioma de traducción, se muestran posibles niveles a elegir
        if ($idioma1 && $idioma2) {
            // petición a base de datos
            $repository = $this->em->getRepository(Filtros::class);
            $nivs = $repository->createQueryBuilder("f")                
                ->select('n.id, n.nombre')
                ->innerJoin('f.nivel', 'n', 'WITH', 'f.nivel = n.id')
                ->andWhere('f.idioma1 = :val1')
                ->andWhere('f.idioma2 = :val2')    
                ->setParameters(array('val1'=> $idioma1->getId(), 'val2' => $idioma2->getId()))
                ->distinct()
                ->getQuery()
                ->getArrayResult()
            ;
                    
            foreach($nivs as $niv) {
                $niveles[] = $this->em->getRepository(Nivel::class)->find($niv['id']);
            }
        }

        
        // Se añaden los idiomas
        $form->add('nivel', EntityType::class, [
                'class' => Nivel::class,
                'choices' => $niveles,
                'choice_label' => function(Nivel $nivel) {
                    return sprintf('%s', ucfirst($nivel->getNombre()));
                },
                'placeholder' => 'Seleccione un nivel',
                'label' => 'Nivel'
        ]);        
    }
    
    

    public function addCategoria(FormInterface $form, Idioma $idioma1 = null, Idioma $idioma2 = null, $nivel = null)
    {
        // No se muestran las posibles categorias hasta que se eliga el nivel del vocabulario
        $categorias = array();
        //var_dump($nivel);
        // si ya se ha elegido el nivel de traducción, se muestran las posibles categorias a elegir
        if ($idioma1 && $idioma2 && $nivel) {
            // petición a base de datos
            $repository = $this->em->getRepository(Filtros::class);
            
            $cats = $repository->createQueryBuilder("f")
                ->select('c.id, c.nombre')
                ->innerJoin('f.categoria', 'c', 'WITH', 'f.categoria = c.id')
                ->andWhere('f.idioma1 = :val1')
                ->andWhere('f.idioma2 = :val2')   
                ->andWhere('f.nivel = :val3')
                ->setParameters(array('val1'=> $idioma1->getId(), 'val2' => $idioma2->getId(), 'val3' => $nivel->getId()))
                ->distinct()
                ->getQuery()
                ->getArrayResult()
            ;
            foreach($cats as $cat) {
                $categorias[] = $this->em->getRepository(Categoria::class)->find($cat['id']);
            }
        }
        
        // Se añaden los idiomas
        $form->add('categoria', EntityType::class, [
                'class' => Categoria::class,
                'choices' => $categorias,
                'choice_label' => function(Categoria $categoria) {
                    return sprintf('%s', ucfirst($categoria->getNombre()));
                },
                'placeholder' => 'Seleccione una categoria',
                'label' => 'Categoría'
        ]);        
    }    
    

    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filtros::class,
            'autor' => null,
            'idiomas' => null
        ]);
    }
}
