<?php

namespace App\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use App\Form\PreguntaType;

use App\Entity\Test;


class TestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('preguntas', CollectionType::class, [
                // colección de objetos de tipo Pregunta
                'entry_type' => PreguntaType::class,
                // opciones de los objetos de tipo Pregunta
                'entry_options' => [
                    'attr' => [],
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Test::class,
        ]);
    }
}
