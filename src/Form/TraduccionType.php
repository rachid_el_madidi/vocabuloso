<?php

namespace App\Form;

use App\Entity\Traduccion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Form\FiltrosType;
use App\Form\PalabraType;

class TraduccionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('palabra1', PalabraType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Título del listado de vocabulario'
                ]
            ])               
            ->add('palabra2', PalabraType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'placeholder' => 'Título del listado de vocabulario'
                ]
            ])               
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Traduccion::class,
        ]);
    }
}
