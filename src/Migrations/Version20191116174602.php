<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191116174602 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE autor (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categoria (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(40) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE idioma (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(40) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nivel (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE palabra (id INT AUTO_INCREMENT NOT NULL, idioma_id_id INT NOT NULL, valor VARCHAR(255) NOT NULL, INDEX IDX_11B8C74A29A35D0 (idioma_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traduccion (id INT AUTO_INCREMENT NOT NULL, palabra_id_id INT NOT NULL, palabra_id2_id INT NOT NULL, autor_id_id INT NOT NULL, nivel_id_id INT NOT NULL, categoria_id_id INT NOT NULL, INDEX IDX_2426C8E9703DA26 (palabra_id_id), INDEX IDX_2426C8E1643045A (palabra_id2_id), INDEX IDX_2426C8E7170846C (autor_id_id), INDEX IDX_2426C8E57CDC211 (nivel_id_id), INDEX IDX_2426C8E7E735794 (categoria_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE palabra ADD CONSTRAINT FK_11B8C74A29A35D0 FOREIGN KEY (idioma_id_id) REFERENCES idioma (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E9703DA26 FOREIGN KEY (palabra_id_id) REFERENCES palabra (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E1643045A FOREIGN KEY (palabra_id2_id) REFERENCES palabra (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E7170846C FOREIGN KEY (autor_id_id) REFERENCES autor (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E57CDC211 FOREIGN KEY (nivel_id_id) REFERENCES nivel (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E7E735794 FOREIGN KEY (categoria_id_id) REFERENCES categoria (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E7170846C');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E7E735794');
        $this->addSql('ALTER TABLE palabra DROP FOREIGN KEY FK_11B8C74A29A35D0');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E57CDC211');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E9703DA26');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E1643045A');
        $this->addSql('DROP TABLE autor');
        $this->addSql('DROP TABLE categoria');
        $this->addSql('DROP TABLE idioma');
        $this->addSql('DROP TABLE nivel');
        $this->addSql('DROP TABLE palabra');
        $this->addSql('DROP TABLE traduccion');
    }
}
