<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191116175618 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE palabra DROP FOREIGN KEY FK_11B8C74A29A35D0');
        $this->addSql('DROP INDEX IDX_11B8C74A29A35D0 ON palabra');
        $this->addSql('ALTER TABLE palabra CHANGE idioma_id_id idioma_id INT NOT NULL');
        $this->addSql('ALTER TABLE palabra ADD CONSTRAINT FK_11B8C74DEDC0611 FOREIGN KEY (idioma_id) REFERENCES idioma (id)');
        $this->addSql('CREATE INDEX IDX_11B8C74DEDC0611 ON palabra (idioma_id)');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E1643045A');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E57CDC211');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E7170846C');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E7E735794');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E9703DA26');
        $this->addSql('DROP INDEX IDX_2426C8E9703DA26 ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8E57CDC211 ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8E1643045A ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8E7E735794 ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8E7170846C ON traduccion');
        $this->addSql('ALTER TABLE traduccion ADD palabra1_id INT NOT NULL, ADD palabra2_id INT NOT NULL, ADD autor_id INT NOT NULL, ADD nivel_id INT NOT NULL, ADD categoria_id INT NOT NULL, DROP palabra_id_id, DROP palabra_id2_id, DROP autor_id_id, DROP nivel_id_id, DROP categoria_id_id');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E229072D9 FOREIGN KEY (palabra1_id) REFERENCES palabra (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E3025DD37 FOREIGN KEY (palabra2_id) REFERENCES palabra (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E14D45BBE FOREIGN KEY (autor_id) REFERENCES autor (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8EDA3426AE FOREIGN KEY (nivel_id) REFERENCES nivel (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E3397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id)');
        $this->addSql('CREATE INDEX IDX_2426C8E229072D9 ON traduccion (palabra1_id)');
        $this->addSql('CREATE INDEX IDX_2426C8E3025DD37 ON traduccion (palabra2_id)');
        $this->addSql('CREATE INDEX IDX_2426C8E14D45BBE ON traduccion (autor_id)');
        $this->addSql('CREATE INDEX IDX_2426C8EDA3426AE ON traduccion (nivel_id)');
        $this->addSql('CREATE INDEX IDX_2426C8E3397707A ON traduccion (categoria_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE palabra DROP FOREIGN KEY FK_11B8C74DEDC0611');
        $this->addSql('DROP INDEX IDX_11B8C74DEDC0611 ON palabra');
        $this->addSql('ALTER TABLE palabra CHANGE idioma_id idioma_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE palabra ADD CONSTRAINT FK_11B8C74A29A35D0 FOREIGN KEY (idioma_id_id) REFERENCES idioma (id)');
        $this->addSql('CREATE INDEX IDX_11B8C74A29A35D0 ON palabra (idioma_id_id)');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E229072D9');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E3025DD37');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E14D45BBE');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8EDA3426AE');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E3397707A');
        $this->addSql('DROP INDEX IDX_2426C8E229072D9 ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8E3025DD37 ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8E14D45BBE ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8EDA3426AE ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8E3397707A ON traduccion');
        $this->addSql('ALTER TABLE traduccion ADD palabra_id_id INT NOT NULL, ADD palabra_id2_id INT NOT NULL, ADD autor_id_id INT NOT NULL, ADD nivel_id_id INT NOT NULL, ADD categoria_id_id INT NOT NULL, DROP palabra1_id, DROP palabra2_id, DROP autor_id, DROP nivel_id, DROP categoria_id');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E1643045A FOREIGN KEY (palabra_id2_id) REFERENCES palabra (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E57CDC211 FOREIGN KEY (nivel_id_id) REFERENCES nivel (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E7170846C FOREIGN KEY (autor_id_id) REFERENCES autor (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E7E735794 FOREIGN KEY (categoria_id_id) REFERENCES categoria (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E9703DA26 FOREIGN KEY (palabra_id_id) REFERENCES palabra (id)');
        $this->addSql('CREATE INDEX IDX_2426C8E9703DA26 ON traduccion (palabra_id_id)');
        $this->addSql('CREATE INDEX IDX_2426C8E57CDC211 ON traduccion (nivel_id_id)');
        $this->addSql('CREATE INDEX IDX_2426C8E1643045A ON traduccion (palabra_id2_id)');
        $this->addSql('CREATE INDEX IDX_2426C8E7E735794 ON traduccion (categoria_id_id)');
        $this->addSql('CREATE INDEX IDX_2426C8E7170846C ON traduccion (autor_id_id)');
    }
}
