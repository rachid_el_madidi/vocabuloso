<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191119013849 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE filtros (id INT AUTO_INCREMENT NOT NULL, autor_id INT NOT NULL, nivel_id INT NOT NULL, categoria_id INT NOT NULL, idioma1_id INT NOT NULL, idioma2_id INT NOT NULL, cantidad SMALLINT NOT NULL, INDEX IDX_9E72BE6414D45BBE (autor_id), INDEX IDX_9E72BE64DA3426AE (nivel_id), INDEX IDX_9E72BE643397707A (categoria_id), INDEX IDX_9E72BE64BC02D167 (idioma1_id), INDEX IDX_9E72BE64AEB77E89 (idioma2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE6414D45BBE FOREIGN KEY (autor_id) REFERENCES autor (id)');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE64DA3426AE FOREIGN KEY (nivel_id) REFERENCES nivel (id)');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE643397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id)');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE64BC02D167 FOREIGN KEY (idioma1_id) REFERENCES idioma (id)');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE64AEB77E89 FOREIGN KEY (idioma2_id) REFERENCES idioma (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE filtros');
    }
}
