<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191119035917 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE filtros DROP FOREIGN KEY FK_9E72BE6414D45BBE');
        $this->addSql('DROP INDEX IDX_9E72BE6414D45BBE ON filtros');
        $this->addSql('ALTER TABLE filtros CHANGE autor_id traduccion_id INT NOT NULL');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE64DC70F7AF FOREIGN KEY (traduccion_id) REFERENCES traduccion (id)');
        $this->addSql('CREATE INDEX IDX_9E72BE64DC70F7AF ON filtros (traduccion_id)');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E14D45BBE');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8E3397707A');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8EDA3426AE');
        $this->addSql('DROP INDEX IDX_2426C8E3397707A ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8E14D45BBE ON traduccion');
        $this->addSql('DROP INDEX IDX_2426C8EDA3426AE ON traduccion');
        $this->addSql('ALTER TABLE traduccion DROP autor_id, DROP nivel_id, DROP categoria_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE filtros DROP FOREIGN KEY FK_9E72BE64DC70F7AF');
        $this->addSql('DROP INDEX IDX_9E72BE64DC70F7AF ON filtros');
        $this->addSql('ALTER TABLE filtros CHANGE traduccion_id autor_id INT NOT NULL');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE6414D45BBE FOREIGN KEY (autor_id) REFERENCES autor (id)');
        $this->addSql('CREATE INDEX IDX_9E72BE6414D45BBE ON filtros (autor_id)');
        $this->addSql('ALTER TABLE traduccion ADD autor_id INT NOT NULL, ADD nivel_id INT NOT NULL, ADD categoria_id INT NOT NULL');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E14D45BBE FOREIGN KEY (autor_id) REFERENCES autor (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8E3397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id)');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8EDA3426AE FOREIGN KEY (nivel_id) REFERENCES nivel (id)');
        $this->addSql('CREATE INDEX IDX_2426C8E3397707A ON traduccion (categoria_id)');
        $this->addSql('CREATE INDEX IDX_2426C8E14D45BBE ON traduccion (autor_id)');
        $this->addSql('CREATE INDEX IDX_2426C8EDA3426AE ON traduccion (nivel_id)');
    }
}
