<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191120204450 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE filtros DROP FOREIGN KEY FK_9E72BE64DC70F7AF');
        $this->addSql('DROP INDEX IDX_9E72BE64DC70F7AF ON filtros');
        $this->addSql('ALTER TABLE filtros DROP traduccion_id');
        $this->addSql('ALTER TABLE traduccion ADD filtros_id INT NOT NULL');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8EEDD23574 FOREIGN KEY (filtros_id) REFERENCES filtros (id)');
        $this->addSql('CREATE INDEX IDX_2426C8EEDD23574 ON traduccion (filtros_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE filtros ADD traduccion_id INT NOT NULL');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE64DC70F7AF FOREIGN KEY (traduccion_id) REFERENCES traduccion (id)');
        $this->addSql('CREATE INDEX IDX_9E72BE64DC70F7AF ON filtros (traduccion_id)');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8EEDD23574');
        $this->addSql('DROP INDEX IDX_2426C8EEDD23574 ON traduccion');
        $this->addSql('ALTER TABLE traduccion DROP filtros_id');
    }
}
