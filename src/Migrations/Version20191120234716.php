<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191120234716 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE palabra DROP FOREIGN KEY FK_11B8C74DEDC0611');
        $this->addSql('DROP INDEX IDX_11B8C74DEDC0611 ON palabra');
        $this->addSql('ALTER TABLE palabra DROP idioma_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE palabra ADD idioma_id INT NOT NULL');
        $this->addSql('ALTER TABLE palabra ADD CONSTRAINT FK_11B8C74DEDC0611 FOREIGN KEY (idioma_id) REFERENCES idioma (id)');
        $this->addSql('CREATE INDEX IDX_11B8C74DEDC0611 ON palabra (idioma_id)');
    }
}
