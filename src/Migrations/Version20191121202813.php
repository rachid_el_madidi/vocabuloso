<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191121202813 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE listado (id INT AUTO_INCREMENT NOT NULL, filtros_id INT NOT NULL, UNIQUE INDEX UNIQ_312779D4EDD23574 (filtros_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE listado ADD CONSTRAINT FK_312779D4EDD23574 FOREIGN KEY (filtros_id) REFERENCES filtros (id)');
        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8EEDD23574');
        $this->addSql('DROP INDEX IDX_2426C8EEDD23574 ON traduccion');
        $this->addSql('ALTER TABLE traduccion CHANGE filtros_id listado_id INT NOT NULL');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8EB7362F39 FOREIGN KEY (listado_id) REFERENCES listado (id)');
        $this->addSql('CREATE INDEX IDX_2426C8EB7362F39 ON traduccion (listado_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE traduccion DROP FOREIGN KEY FK_2426C8EB7362F39');
        $this->addSql('DROP TABLE listado');
        $this->addSql('DROP INDEX IDX_2426C8EB7362F39 ON traduccion');
        $this->addSql('ALTER TABLE traduccion CHANGE listado_id filtros_id INT NOT NULL');
        $this->addSql('ALTER TABLE traduccion ADD CONSTRAINT FK_2426C8EEDD23574 FOREIGN KEY (filtros_id) REFERENCES filtros (id)');
        $this->addSql('CREATE INDEX IDX_2426C8EEDD23574 ON traduccion (filtros_id)');
    }
}
