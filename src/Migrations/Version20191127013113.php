<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191127013113 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE filtros_idioma');
        $this->addSql('ALTER TABLE filtros ADD idioma1_id INT NOT NULL, ADD idioma2_id INT NOT NULL');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE64BC02D167 FOREIGN KEY (idioma1_id) REFERENCES idioma (id)');
        $this->addSql('ALTER TABLE filtros ADD CONSTRAINT FK_9E72BE64AEB77E89 FOREIGN KEY (idioma2_id) REFERENCES idioma (id)');
        $this->addSql('CREATE INDEX IDX_9E72BE64BC02D167 ON filtros (idioma1_id)');
        $this->addSql('CREATE INDEX IDX_9E72BE64AEB77E89 ON filtros (idioma2_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE filtros_idioma (filtros_id INT NOT NULL, idioma_id INT NOT NULL, INDEX IDX_654F8644EDD23574 (filtros_id), INDEX IDX_654F8644DEDC0611 (idioma_id), PRIMARY KEY(filtros_id, idioma_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE filtros_idioma ADD CONSTRAINT FK_654F8644DEDC0611 FOREIGN KEY (idioma_id) REFERENCES idioma (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filtros_idioma ADD CONSTRAINT FK_654F8644EDD23574 FOREIGN KEY (filtros_id) REFERENCES filtros (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filtros DROP FOREIGN KEY FK_9E72BE64BC02D167');
        $this->addSql('ALTER TABLE filtros DROP FOREIGN KEY FK_9E72BE64AEB77E89');
        $this->addSql('DROP INDEX IDX_9E72BE64BC02D167 ON filtros');
        $this->addSql('DROP INDEX IDX_9E72BE64AEB77E89 ON filtros');
        $this->addSql('ALTER TABLE filtros DROP idioma1_id, DROP idioma2_id');
    }
}
