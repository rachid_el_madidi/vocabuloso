<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129201940 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pregunta (id INT AUTO_INCREMENT NOT NULL, test_id INT NOT NULL, traduccion_id INT NOT NULL, respuesta VARCHAR(255) NOT NULL, INDEX IDX_AEE0E1F71E5D0459 (test_id), INDEX IDX_AEE0E1F7DC70F7AF (traduccion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test (id INT AUTO_INCREMENT NOT NULL, filtros_id INT NOT NULL, creado DATETIME NOT NULL, realizado DATETIME NOT NULL, nota NUMERIC(4, 2) NOT NULL, INDEX IDX_D87F7E0CEDD23574 (filtros_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pregunta ADD CONSTRAINT FK_AEE0E1F71E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE pregunta ADD CONSTRAINT FK_AEE0E1F7DC70F7AF FOREIGN KEY (traduccion_id) REFERENCES traduccion (id)');
        $this->addSql('ALTER TABLE test ADD CONSTRAINT FK_D87F7E0CEDD23574 FOREIGN KEY (filtros_id) REFERENCES filtros (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pregunta DROP FOREIGN KEY FK_AEE0E1F71E5D0459');
        $this->addSql('DROP TABLE pregunta');
        $this->addSql('DROP TABLE test');
    }
}
