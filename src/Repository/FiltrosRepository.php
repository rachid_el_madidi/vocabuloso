<?php

namespace App\Repository;

use App\Entity\Filtros;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

use App\Entity\Idioma;
use App\Entity\Nivel;
use App\Entity\Categoria;
use App\Entity\Autor;

/**
 * @method Filtros|null find($id, $lockMode = null, $lockVersion = null)
 * @method Filtros|null findOneBy(array $criteria, array $orderBy = null)
 * @method Filtros[]    findAll()
 * @method Filtros[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FiltrosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Filtros::class);
    }

    /**
     * @return Filtros[] Returns an array of Filtros objects
     */

    public function palabrasPorNivel()
    {
        return $this->createQueryBuilder('f')  
            ->select(array('SUM(f.cantidad) as cantidad','n.nombre'))
            ->innerJoin('f.nivel', 'n', 'WITH', 'f.nivel = n.id')
            ->groupBy('f.nivel')
            ->getQuery()
            ->getArrayResult()
        ;
    }


    /**
     * @return Filtros[] Returns an array of Filtros objects
     */

    public function traduccionesPorIdiomas()
    {
        return $this->createQueryBuilder('f')  
            ->select(array('SUM(f.cantidad) as cantidad','i1.id', 'i2.nombre'))
            ->innerJoin('f.idioma1', 'i1', 'WITH', 'f.idioma1 = i1.id')
            ->innerJoin('f.idioma2', 'i2', 'WITH', 'f.idioma2 = i2.id')
            ->groupBy('f.idioma1')
            ->groupBy('f.idioma2')
            ->getQuery()
            ->getArrayResult()
        ;
    }   
    
    public function idioma1PorAutor($autor)
    {
        return $this->createQueryBuilder('f')
                ->select('i.id')
                ->innerJoin('f.idioma1', 'i', 'WITH', 'f.idioma1 = i.id')
                ->andWhere('f.autor = :val')
                ->setParameter('val', $autor)
                ->distinct()
                ->getQuery()
                ->getResult()
        ;
    }
    
    function findFiltrosRepasar(Idioma $idioma1 = null, Idioma $idioma2 = null, Nivel $nivel = null, Categoria $categoria = null, Autor $autor = null)
    {
        // si ya se han elegido el nivel, los idiomas y la categoría, se busca a qué único filtro pertenecen
        if ($idioma1 && $idioma2 && $nivel && $categoria && $autor) {
            return $this->createQueryBuilder("f")
                ->select("f.id")
                ->where("f.idioma1 = :idioma1_id", "f.idioma2 = :idioma2_id", "f.nivel = :nivel", "f.categoria = :categoria", "f.autor = :autor")
                ->setParameters(array("idioma1_id" => $idioma1->getId(), "idioma2_id" => $idioma2->getId(), "nivel" => $nivel->getId(), "categoria" => $categoria->getId(), "autor" => $autor->getId()))
                ->getQuery()
                ->getResult()
            ;
        } else {
            return false;
        }
    }
    
    public function findByAutorId(int $value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.autor = :val')
            ->setParameter('val', $value)
            ->distinct()
            ->getQuery()
            ->getArrayResult()
        ;
    }    
    
    public function getIdioma2ByIdioma1(int $idioma1, int $autor)
    {
        return $this->createQueryBuilder("f")                
            ->select('i.id, i.nombre')
            ->innerJoin('f.idioma2', 'i', 'WITH', 'f.idioma2 = i.id')
            ->andWhere('f.idioma1 = :val1')
            ->andWhere('f.autor = :val2')    
            ->setParameters(array('val1'=> $idioma1, 'val2' => $autor))
            ->distinct()
            ->getQuery()
            ->getArrayResult()
        ;
    }
    
    public function getNiveles(String $idioma1, String $idioma2)
    {
        return $this->createQueryBuilder("f")
            ->select('n.id, n.nombre')
            ->innerJoin('f.nivel', 'n', 'WITH', 'f.nivel = n.id')
            ->andWhere('f.idioma1 = :val1')
            ->andWhere('f.idioma2 = :val2')    
            ->setParameters(array('val1'=> $idioma1, 'val2' => $idioma2))
            ->distinct()
            ->getQuery()
            ->getArrayResult()
        ;
                
    }
    
    
    public function getCategorias(String $idioma1, String $idioma2, String $nivel)
    {
        return $this->createQueryBuilder("f")
            ->select('c.id, c.nombre')
            ->innerJoin('f.categoria', 'c', 'WITH', 'f.categoria = c.id')
            ->andWhere('f.idioma1 = :val1')
            ->andWhere('f.idioma2 = :val2') 
            ->andWhere('f.nivel = :nivel')
            ->setParameters(array('val1'=> $idioma1, 'val2' => $idioma2, 'nivel' => $nivel))
            ->getQuery()
            ->getArrayResult()
        ;
    }
    
}
