<?php

namespace App\Repository;

use App\Entity\Listado;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Listado|null find($id, $lockMode = null, $lockVersion = null)
 * @method Listado|null findOneBy(array $criteria, array $orderBy = null)
 * @method Listado[]    findAll()
 * @method Listado[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListadoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Listado::class);
    }

    public function findOneByFiltrosId($value): ?Listado
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.filtros = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
