<?php

namespace App\Repository;

use App\Entity\Palabra;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Palabra|null find($id, $lockMode = null, $lockVersion = null)
 * @method Palabra|null findOneBy(array $criteria, array $orderBy = null)
 * @method Palabra[]    findAll()
 * @method Palabra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PalabraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Palabra::class);
    }
}
