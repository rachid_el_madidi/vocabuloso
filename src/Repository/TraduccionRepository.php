<?php

namespace App\Repository;

use App\Entity\Traduccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\Listado;

/**
 * @method Traduccion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Traduccion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Traduccion[]    findAll()
 * @method Traduccion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TraduccionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Traduccion::class);
    }

    /**
     * @return Traduccion[] Returns an array of Traduccion objects
     */
    public function palabrasAlAzar( int $numPalabras )
    {
        return $this->createQueryBuilder( 'q' )
                ->orderby( 'RAND()' )
                ->setMaxResults( $numPalabras )
                ->getQuery()
                ->getResult()  
        ;
    }
    
    /**
     * Devuelve un array con las traducciones de un mismo listado
     * @param int $listado
     * @return Traduccion[]
     */
    public function getTradByListId(Listado $listado) {
        return $this->createQueryBuilder( 't' )
                ->andWhere("t.listado = :listId")
                ->setParameter("listId", $listado->getId())
                ->getQuery()
                ->getResult()
        ;
    }
}
